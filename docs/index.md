# Welcome to Our Sci's Literature Review Documentation

## Purpose

This is a static webpage that houses all Our Sci literature review questions, conclusions, and sources. The page was created so that searching through previous literature reviews is more efficient. Rather than needing to look through DropBox, Google Drive, GitLab, and Zotero, you should be able to find any previously-conducted research and the relevant sources on this page. We hope this helps! 

## Structure

You will see on the left hand table of contents that this repository is organized by research topic (for previously-conducted research) and by alphabetical order (for previously-sited sources). Please maintain this organization when adding new content to the repository. Additionally, we ask that you note the DOI for any sources that you add to this repository. 

## Accessing Referenced Sources 

If you need to access previously sited sources, we recommend [Sci-Hub](https://sci-hub.se/). However, please note that anything you access here is at your own risk. If you work within Our Sci and would prefer to access a source via a more secure method, use a VPN. If you don't have a VPN ask Dan, Nat, or someone else with access to an educational institution's virtual network.